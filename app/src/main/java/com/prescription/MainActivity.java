package com.prescription;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    Button generatepdf;
    EditText ed_bill,ed_date,ed_patientname,ed_age,ed_sex,ed_reg,ed_bedno,ed_add,ed_dateaddmision,ed_datedischarge,ed_diagonasis,ed_charenicu,ed_charenursing,ed_charedoctorround,ed_chareoxygen,
            ed_charecardicmonitor,ed_charesyringepump,ed_charerbs,ed_grandtotal,ed_paidamount;
    int pagewidth =1200;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ed_bill = findViewById(R.id.ed_bill);
        ed_date = findViewById(R.id.ed_date);
        ed_patientname = findViewById(R.id.ed_patientname);
        ed_age = findViewById(R.id.ed_age);
        ed_sex = findViewById(R.id.ed_sex);
        ed_reg = findViewById(R.id.ed_reg);
        ed_bedno = findViewById(R.id.ed_bedno);
        ed_add = findViewById(R.id.ed_add);
        ed_dateaddmision = findViewById(R.id.ed_dateaddmision);
        ed_datedischarge = findViewById(R.id.ed_datedischarge);
        ed_diagonasis = findViewById(R.id.ed_diagonasis);
        ed_charenicu = findViewById(R.id.ed_charenicu);
        ed_charenursing = findViewById(R.id.ed_charenursing);
        ed_charedoctorround = findViewById(R.id.ed_charedoctorround);
        ed_chareoxygen = findViewById(R.id.ed_chareoxygen);
        ed_charecardicmonitor = findViewById(R.id.ed_charecardicmonitor);
        ed_charesyringepump = findViewById(R.id.ed_charesyringepump);
        ed_grandtotal = findViewById(R.id.ed_grandtotal);
        ed_charerbs = findViewById(R.id.ed_charerbs);
        ed_paidamount = findViewById(R.id.ed_paidamount);
        generatepdf = findViewById(R.id.bt_generatepdf);

        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        generatepdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calling method to
                // generate our PDF file.

                createPOF();
                Toast.makeText(getApplicationContext(),"Hello ",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createPOF() {
        PdfDocument myPdfDocument = new PdfDocument();
        Paint mypaint = new Paint();
        Paint titlePaint = new Paint();

        PdfDocument.PageInfo mypageInfo = new PdfDocument.PageInfo.Builder(1200, 1700, 1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(mypageInfo);
        Canvas canvas = myPage.getCanvas();

        titlePaint.setTextAlign(Paint.Align.LEFT);
        titlePaint.setTypeface(Typeface.create(Typeface.DEFAULT,Typeface.BOLD));
        titlePaint.setTextSize(40);

        mypaint.setTextAlign(Paint.Align.LEFT);
        mypaint.setTypeface(Typeface.create(Typeface.DEFAULT,Typeface.BOLD));
        mypaint.setTextSize(30);

        canvas.drawText("DR VIVEKANAND PAUL CLINIC  ",40,100,titlePaint);
        canvas.drawText("VIP ROAD, DARBHANGA",40,150,titlePaint);

        canvas.drawText("BILL NO.: "+ed_bill.getText(),40,270,mypaint);
        canvas.drawText("DATE: "+ed_date.getText(),pagewidth/2,270,mypaint);

        canvas.drawText("NAME OF THE PATIENT: "+ed_patientname.getText(),40,370,mypaint);

        canvas.drawText("AGE: "+ed_age.getText(),40,470,mypaint);
        canvas.drawText("SEX: "+ed_sex.getText(),pagewidth/2,470,mypaint);

        canvas.drawText("REG NO: "+ed_reg.getText(),40,570,mypaint);
        canvas.drawText("BED NO. NICU: "+ed_bedno.getText(),pagewidth/2,570,mypaint);

        canvas.drawText("ADDRESS: "+ed_add.getText(),40,670,mypaint);

        canvas.drawText("DATE OF ADMISSION: "+ed_dateaddmision.getText(),40,770,mypaint);
        canvas.drawText("DATE OF DISCHARGE: "+ed_datedischarge.getText(),pagewidth/2,770,mypaint);

        canvas.drawText("NAME OF THE TREATING DOCTOR : DR Vivekanand paul,MBBS,MD(Pediayrics),DCH",40,870,mypaint);

        canvas.drawText("DIAGNOSIS: "+ed_diagonasis.getText(),40,970,mypaint);

        canvas.drawText("CHARGES: ",40,1070,mypaint);

        canvas.drawText("1. NICU BED CHARGE: "+ed_charenicu.getText(),140,1170,mypaint);
        canvas.drawText("2. NURSING CHARGE: "+ed_charenursing.getText(),140,1270,mypaint);
        canvas.drawText("3. DOCTOR ROUND CHARGE: "+ed_charedoctorround.getText(),140,1370,mypaint);
        canvas.drawText("4. OXYGEN CHARGE:: "+ed_chareoxygen.getText(),140,1470,mypaint);

        myPdfDocument.finishPage(myPage);

        PdfDocument.PageInfo mypageInfo1 = new PdfDocument.PageInfo.Builder(1200, 1700, 1).create();
        PdfDocument.Page myPage1 = myPdfDocument.startPage(mypageInfo1);
        Canvas canvas1 = myPage1.getCanvas();


        canvas1.drawText("5.CARDIC MONITOR CHARGE: "+ed_charecardicmonitor.getText(),140,250,mypaint);
        canvas1.drawText("6. SYRINGE PUMP CHARGE: "+ed_charesyringepump.getText(),140,350,mypaint);
        canvas1.drawText("7. RBS: "+ed_charerbs.getText(),140,450,mypaint);


        myPdfDocument.finishPage(myPage1);


        File file = new File(Environment.getExternalStorageDirectory(),"/first.pdf");
        try{
            myPdfDocument.writeTo(new FileOutputStream(file));
        }catch (IOException e){
            e.printStackTrace();
        }

        myPdfDocument.close();
    }
}